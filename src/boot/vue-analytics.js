import VueAnalytics from "vue-analytics";

export default async ({ router, Vue }) => {

	Vue.use(VueAnalytics, {
		id: "UA-130868344-2",
		router,
		debug: {
			sendHitTask: process.env.NODE_ENV === "production"
		}
	});
};
