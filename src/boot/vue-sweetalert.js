import Vue from "vue";
import VueSweetalert2 from "vue-sweetalert2";

Vue.use(VueSweetalert2, {
	showCancelButton: true,
	confirmButtonColor: "#3085d6",
	cancelButtonColor: "#d33",
	reverseButtons: true,
	focusCancel: true,
	icon: "warning",
	target: "div#q-app"
});
