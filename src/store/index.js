import Vue from "vue";
import Vuex from "vuex";

import state from "./global/state";
import * as getters from "./global/getters";
import * as mutations from "./global/mutations";
import * as actions from "./global/actions";
import partiture from "./partiture";

Vue.use(Vuex);

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function (/* { ssrContext } */) {
	const Store = new Vuex.Store({
		getters,
		mutations,
		actions,
		state,
		modules: {
			partiture
		}

		// enable strict mode (adds overhead!)
		// for dev mode only
		// strict: process.env.DEV
	});

	return Store;
}
