export function updateLocate(context, newLocale) {
	context.commit("updateLocate", newLocale);
}

export function updateDrawer(context, newValue) {
	context.commit("updateDrawer", newValue);
}
