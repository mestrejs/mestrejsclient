export function updateLocate(state, newLocale) {
	state.locale = newLocale;
}

export function updateDrawer(state, newValue) {
	state.showDrawer = newValue;
}
