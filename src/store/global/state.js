import { Platform } from "quasar";

export default function () {
	return {
		showDrawer: Platform.is.desktop,
		locale: {
			locale: "es",
			text: "titleBar.localeText.es"
		},
		languages: [
			{
				locale: "es",
				text: "titleBar.localeText.es"
			},
			{
				locale: "en",
				text: "titleBar.localeText.en"
			}
		]
	};
}
