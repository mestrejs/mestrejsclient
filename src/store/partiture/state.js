import { Howl } from "howler";

export default function () {
	return {
		listInstrument: [
			{
				name: "Alfaia",
				possibleNotes: [
					{
						icon: "",
						files: []
					},
					{
						icon: "mdiClose",
						howls: new Howl({ src: ["statics/sounds/alfaia/upperNote.mp3", "statics/sounds/alfaia/upperNote.wav"], preload: true })
					},
					{
						icon: "mdiMinus",
						howls: new Howl({ src: ["statics/sounds/alfaia/lowerNote.mp3", "statics/sounds/alfaia/lowerNote.wav"], preload: true })
					}
				]
			},
			{
				name: "Caixa",
				possibleNotes: [
					{
						icon: "",
						files: []
					},
					{
						icon: "mdiClose",
						howls: new Howl({ src: ["statics/sounds/caixa/upperNote.mp3", "statics/sounds/caixa/upperNote.wav"], preload: true })
					},
					{
						icon: "mdiMinus",
						howls: new Howl({ src: ["statics/sounds/caixa/lowerNote.mp3", "statics/sounds/caixa/lowerNote.wav"], preload: true })
					}
				]
			},
			{
				name: "Gonge",
				possibleNotes: [
					{
						icon: "",
						files: []
					},
					{
						icon: "mdiClose",
						howls: new Howl({ src: ["statics/sounds/gonge/upperNote.mp3", "statics/sounds/gonge/upperNote.wav"], preload: true })
					},
					{
						icon: "mdiMinus",
						howls: new Howl({ src: ["statics/sounds/gonge/lowerNote.mp3", "statics/sounds/gonge/lowerNote.wav"], preload: true })
					}
				]
			},
			{
				name: "Repique",
				possibleNotes: [
					{
						icon: "",
						files: []
					},
					{
						icon: "mdiClose",
						howls: new Howl({ src: ["statics/sounds/repique/upperNote.mp3", "statics/sounds/repique/upperNote.wav"], preload: true })
					},
					{
						icon: "mdiMinus",
						howls: new Howl({ src: ["statics/sounds/repique/lowerNote.mp3", "statics/sounds/repique/lowerNote.wav"], preload: true })
					},
					{
						icon: "mdiCircleOutline",
						howls: new Howl({ src: ["statics/sounds/repique/mediumNote.mp3", "statics/sounds/repique/mediumNote.wav"], preload: true })
					}
				]
			},
			{
				name: "Surdo",
				possibleNotes: [
					{
						icon: "",
						files: []
					},
					{
						icon: "mdiClose",
						howls: new Howl({ src: ["statics/sounds/surdo/upperNote.mp3", "statics/sounds/surdo/upperNote.wav"], preload: true })
					},
					{
						icon: "mdiCheckboxBlankCircle",
						howls: new Howl({ src: ["statics/sounds/surdo/mediumNote.mp3", "statics/sounds/surdo/mediumNote.wav"], preload: true })
					},
					{
						icon: "mdiMinus",
						howls: new Howl({ src: ["statics/sounds/surdo/lowerNote.mp3", "statics/sounds/surdo/lowerNote.wav"], preload: true })
					}
				]
			},
			{
				name: "Timba",
				possibleNotes: [
					{
						icon: "",
						files: []
					},
					{
						icon: "mdiClose",
						howls: new Howl({ src: ["statics/sounds/timba/upperNote.mp3", "statics/sounds/timba/upperNote.wav"], preload: true })
					},
					{
						icon: "mdiCookie",
						howls: new Howl({ src: ["statics/sounds/timba/mediumNote.mp3", "statics/sounds/timba/mediumNote.wav"], preload: true })
					},
					{
						icon: "mdiMinus",
						howls: new Howl({ src: ["statics/sounds/timba/lowerNote.mp3", "statics/sounds/timba/lowerNote.wav"], preload: true })
					}
				]
			}
		],
		panzoomInstance: null,
		bpmActual: 90,
		showNotes: false
	};
}
