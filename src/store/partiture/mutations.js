export function updateLocate(state, newLocale) {
	state.locale = newLocale;
}

export function updateBPM(state, newBPM) {
	state.bpmActual = newBPM;
}

export function updateShowNotes(state, value) {
	state.showNotes = value;
}

export function setPanzoomInstance(state, instance) {
	state.panzoomInstance = instance;
}
