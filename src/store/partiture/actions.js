export function updateLocate(context, newLocale) {
	context.commit("updateLocate", newLocale);
}

export function updateBPM(context, newBPM) {
	context.commit("updateBPM", newBPM);
}

export function updateShowNotes(context, value) {
	context.commit("updateShowNotes", value);
}

export function setPanzoomInstance(context, instance) {
	context.commit("setPanzoomInstance", instance);
}
