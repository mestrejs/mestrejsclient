export function getPossibleNotes(state) {
	return (type) => state.listInstrument[type].possibleNotes;
}

export function getNameIntrument(state) {
	return (type) => state.listInstrument[type].name;
}

export function getIconNoteInstrument(state) {
	return (type, indexNote) => state.listInstrument[type].possibleNotes[indexNote].icon;
}
