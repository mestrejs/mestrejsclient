<div align="center">
  <a href="" rel="noopener">
  <img width=200px height=200px src="https://mestrejs.com/img/icons/icon-512x512.png" alt="Project logo"></a>


  <h3 align="center">MestreJS</h3>
</div>

<div align="center">

  [![Status](https://img.shields.io/badge/status-active-success.svg?style=flat-square&logo=gitlab)]()
  [![Netlify Status](https://api.netlify.com/api/v1/badges/53af11e5-540a-449d-9f45-af11b5e79e2a/deploy-status)](https://mestrejs.com/)
  [![License](https://img.shields.io/badge/license-GNU_GPLv3-blue.svg)](/LICENSE)
  <!-- [![GitHub Pull Requests](https://img.shields.io/github/issues-pr/kylelobo/The-Documentation-Compendium.svg)](https://github.com/kylelobo/The-Documentation-Compendium/pulls) -->
  <!-- [![GitHub Issues](https://img.shields.io/github/issues/kylelobo/The-Documentation-Compendium.svg)](https://github.com/kylelobo/The-Documentation-Compendium/issues) -->

</div>

---

<p align="center"> Transform your click in brazilian rhythms in a beautiful web app.
    <br>
</p>

## 📝 Table of Contents
- [About](#about)
- [Getting Started](#getting_started)
- [Deployment](#deployment)
- [Built Using](#built_using)
- [Authors](#authors)
- [Acknowledgments](#acknowledgement)
<!-- - [Usage](#usage) -->
<!-- - [TODO](../TODO.md) -->
<!-- - [Contributing](../CONTRIBUTING.md) -->

## 🧐 About <a name = "about"></a>
MestreJS allows you to create brazilian music for free, only making a few clicks and listening your creations, in all plataforms and with a nice UI.

## 🏁 Getting Started <a name = "getting_started"></a>
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See [deployment](#deployment) for notes on how to deploy the project on a live system.

<!-- ### Prerequisites
What things you need to install the software and how to install them.

```
Give examples
``` -->

### Installing
The needed steps to make a development enviroment.

1. First, download all the needed libraries:
	```
	yarn install
	```
2. Get a development server run:
	```
	yarn serve
	```
3. Access the server page [http://localhost:8080/#](http://localhost:8080/#).

<!-- ## 🔧 Running the tests <a name = "tests"></a>
Explain how to run the automated tests for this system.

### Break down into end to end tests
Explain what these tests test and why

```
Give an example
```

### And coding style tests
Explain what these tests test and why

```
Give an example
``` -->

<!-- ## 🎈 Usage <a name="usage"></a>
Add notes about how to use the system. -->

## 🚀 Deployment <a name = "deployment"></a>
Steps to generate a production-ready app.

1. First, download all the needed libraries:
	```
	yarn install
	```
2. Execute build:
	```
	yarn run build
	```
3. Copy the `dist/pwa` folder in root of your web server


## ⛏️ Built Using <a name = "built_using"></a>
- [VueJs](https://vuejs.org/) - Web Framework
- [i18n](https://kazupon.github.io/vue-i18n/) - Internationalization Framework
- [Vuetify](https://vuetifyjs.com/en/) - Material Design Framework
- [Hammer](http://hammerjs.github.io/) - Gestures Handle
- [SweetAlert2](https://sweetalert2.github.io/) - Beautifuls dialogs

## ✍️ Authors <a name = "authors"></a>
- [@nisgrak](https://gitlab.com/Nisgrak) - Idea & Initial work

<!-- See also the list of [contributors](https://github.com/kylelobo/The-Documentation-Compendium/contributors) who participated in this project. -->

## 🎉 Acknowledgements <a name = "acknowledgement"></a>
- [Asociación Cultural Ymalê](https://www.ymale.es/) - Inspirate me and iniciate in music world
<!-- - Hat tip to anyone whose code was used
- Inspiration
- References -->
